//
//  fichier.cpp
//  filePatch
//
//  Created by Brice Rivé on 07/10/12.
//  Copyright (c) 2012 Brice Rivé. All rights reserved.
//

#include "fichier.h"
#include "error.h"
#include <iostream>

static struct dirent **filelist = {0};
static int filelistIdx=0;

int FIC::dir_open(const char *d, const char *m, int *n)
{
    int fcount = scandir(d, &filelist, 0, alphasort);
    
    if (fcount < 0)
    {
        ERR_EXIT("No file found");
    }
    filelistIdx=0;
    
    *n=fcount;
    ERR_OK();
}

int FIC::dir_next(char *d)
{
    strcpy(d, filelist[filelistIdx++]->d_name);
    ERR_OK();
}

void FIC::dir_close()
{
    free(filelist);
}
