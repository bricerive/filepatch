//
//  patch.cpp
//  filePatch
//
//  Created by Brice Rivé on 07/10/12.
//  Copyright (c) 2012 Brice Rivé. All rights reserved.
//

#include "error.h"
#include "fichier.h"
#include <boost/dll.hpp>
#include <boost/filesystem.hpp>
#include <string.h>
//#define NOMACROS
#include <curses.h>
#include <iostream>
#include <cctype>

using namespace std;
namespace fs = boost::filesystem;

struct FileView;

void theAppl(WINDOW *, int argc, char *argv[]);

static const int MESS_NL=1;
static const int HEX_COL=12;
static const int ASC_COL=62;

typedef struct {
    int l,c,nl,nc;
} WIN_DEF;

struct Layout {
    Layout(WINDOW *win)
    {
        static const int WIN_NL_MIN=24;
        static const int WIN_NL_MAX=24;
        static const int WIN_NC_MIN=80;
        static const int WIN_NC_MAX=80;
        
        static const int DISP_LIG=0;
        static const int DISP_COL=0;
        
        static const int DIAL_NL=4;
        static const int DIAL_NC=42;
        
        static const int HELP_NL=19;
        static const int HELP_NC=78;
        
        static const int DIR_NL=15;
        static const int DIR_NC=20;
        
        getbegyx(win, win_l, win_c);
        getmaxyx(win, win_nl, win_nc);
        if (win_nl<WIN_NL_MIN || win_nc<WIN_NC_MIN)
            FAIL("Screen too small");
        
        if (win_nl>WIN_NL_MAX)
            win_nl = WIN_NL_MAX;
        if (win_nc>WIN_NC_MAX)
            win_nc = WIN_NC_MAX;
        
        disp.nl = win_nl;
        disp.nc = win_nc;
        disp.l = win_l+DISP_LIG;
        disp.c = win_c+DISP_COL;
        
        help.nl = HELP_NL;
        help.nc = HELP_NC;
        help.l = (disp.l*2+disp.nl-help.nl)/2;
        help.c = (disp.c*2+disp.nc-help.nc)/2;
        
        dial.nl = DIAL_NL;
        dial.nc = DIAL_NC;
        dial.l = (disp.l*2+disp.nl-dial.nl)/2;
        dial.c = (disp.c*2+disp.nc-dial.nc)/2;
        
        dir.nl = DIR_NL;
        dir.nc = DIR_NC;
        dir.l = (disp.l*2+disp.nl-dir.nl)/2;
        dir.c = (disp.c*2+disp.nc-dir.nc)/2;
    }
    
    int win_l, win_c, win_nl, win_nc;
    WIN_DEF disp;
    WIN_DEF dial;
    WIN_DEF help;
    WIN_DEF dir;
};

class Gui;

WINDOW *WindowCreate(const WIN_DEF &);
static void Edit(Gui &gui, FileView &fv, int &addr, int key);
static int Refresh(Gui &gui, FileView &fv, int &addr);
bool TextEdit(WINDOW *window, int row, int col, char *text, int nbchar);
int WaitForKey(WINDOW *window, const char *set);

static int ToHex(int c);


//

const char *help[] = {
    ""
    ,"Fonctions du patcher de fichiers (Appelees avec CTRL)"
    ,""
    ,"Deplacements:                        Manips fichier:"
    ,"^U -> Up                             ^O -> Other file"
    ,"^D -> Down                           ^W -> Write sector"
    ,"^B -> Backward                       ^R -> Re-read sector"
    ,"^F -> Forward                        ^L -> List files"
    ,"^N -> Next page                      ^A -> Abort modifs"
    ,"^P -> Previous page"
    ,"^E -> End of file"
    ,"^G -> Goto position"
    ,"^S -> Search data"
    ,"^T -> Toggle mode (ASCII/Hexa)       Divers:"
    ,"^M -> Mark position                  ^Q -> Quit"
    ,"^J -> Jump to mark                   ^H -> Help"
    ,""
    ,""
    ,""
};

int main(int argc, char *argv[])
{
   fs::current_path(boost::dll::program_location().parent_path());

    // Init curses
    initscr();
    raw();
    noecho();
    nonl();
    curs_set(1);
    

    // Run
    try {
        theAppl(stdscr, argc, argv);
        endwin();
    } catch (Err &err) {
        endwin();
        cout << "Fatal exception: " << endl << err.mText << endl;
    }
    
}

class Gui
{
public:
    Gui(WINDOW *mainwin)
    : layout(mainwin), mainWin(WindowCreate(layout.disp)), msgRow(layout.disp.nl-2), mess(false)
    {
        mvwaddstr(mainWin,1,1,"File:");
        mvwaddstr(mainWin,1,layout.disp.nc-12,"^H -> Help");
        mvwaddstr(mainWin,2,layout.disp.nc-12,"^Q -> Quit");
        wrefresh(mainWin);
    }
    ~Gui()
    {
        delwin(mainWin);
        refresh();
        
    }
    void Mess(const char *str)
    {
        wattron(mainWin,A_BOLD);
        mvwaddstr(mainWin,msgRow,1,str);
        wattroff(mainWin,A_BOLD);
        wrefresh(mainWin);
        mess=true;
    }
    
    void ClrMess()
    {
        if (mess) 
        {
            mvwaddstr(mainWin,msgRow,1,"                                                                                ");
            wrefresh(mainWin);
            mess=false;
        }
    }
    
    Layout layout;
    WINDOW *mainWin;
private:
    int msgRow;
    bool mess;
};

static int GetKey(WINDOW *window)
{
    int key;
    while ((key=wgetch(window))==ERR)
    {
        usleep(10000);
    }
    return key;
}

struct FileView
{
    FileView(): mDirty(false), mAscii(false) {}
    
    bool IsDirtyAndReset() { bool val=mDirty; mDirty=false; return val; }
    void Dirty() {mDirty=true;}
    bool Ascii()const {return mAscii;}
    void ToggleAscii() {mAscii^=1;}
    void Open(const fs::path &filePath) {
        path = filePath;
        if (!(file=fopen(path.c_str(),"rb+"))) {
            /*MESS("Cannot open file in write mode");*/
            if ((file=fopen(path.c_str(),"rb")))
                return;
            FAIL("Open fail");
        }
        file_size = fs::file_size(path);
    }
    const char *FileName()const {return path.filename().c_str();}
    void Read(long offset,char *dst,int n,int *res)
    {
        if (fseek(file,offset,SEEK_SET))
            FAIL("seek error");
        *res=fread(dst,1,n,file);
    }

    int Write(long offset,char *dst,int n)
    {
        fseek(file,offset,SEEK_SET);
        fwrite(dst,n,1,file);
        ERR_OK();
    }

    void Close() {if (file) fclose(file); file=0;}
    fs::path Path()const {return path;}
    long Size()const {return file_size;}
    int addr;
    int mark;
    char data[512];

private:
    FILE *file;
    long file_size;
    fs::path path;
    bool mDirty;
    bool mAscii;
};

static void SwitchToFile(const fs::path &filePath, FileView &fv)
{
    fs::path tempFile = fs::temp_directory_path() / "patch.tmp";
    if (fs::exists(tempFile))
        fs:remove(tempFile);
    fs::copy_file(filePath, tempFile);
    fv.Open(filePath);
    fv.mark = -1;
    fv.Dirty();
    fv.addr = 0;
}

static void OtherFile(Gui &gui, FileView &fv)
{
    WINDOW *mainWin(gui.mainWin);
    
	char dirName[81];
	char fileName[80];
    
    WINDOW *dial_fen=WindowCreate(gui.layout.dial);
    bool ok=false;
    do {
        mvwaddstr(dial_fen,1,1,"Nom de la directory:");
        dirName[0] = 0;
        if ((ok=TextEdit(dial_fen,2,1,dirName,40))) {
            mvwaddstr(dial_fen,1,1,"Nom du fichier:      ");
            fileName[0] = 0;
            ok = TextEdit(dial_fen,2,1,fileName,40);
        }
    } while (ok && !fs::exists(fs::path(dirName)/fileName));
    if (ok) {
        SwitchToFile(fs::path(dirName)/fileName, fv);
        mvwaddstr(mainWin,1,6,"                                                 ");
        mvwaddstr(mainWin,1,6,fv.FileName());
    }
    delwin(dial_fen);
    touchwin(mainWin);
    wrefresh(mainWin);
}

static void ListFiles(Gui &gui)
{
    WINDOW *mainWin(gui.mainWin);
	char dirName[81];
	char mask[80];
    bool ok;
    int val;
    int tmp;
    
    WINDOW *dial_fen=WindowCreate(gui.layout.dial);
    do {
        mvwaddstr(dial_fen,1,1,"Nom de la directory:");
        dirName[0] = 0;
        ok = TextEdit(dial_fen,2,1,dirName,38);
        if (ok) {
            mvwaddstr(dial_fen,1,1,"Masque :            ");
            mask[0] = 0;
            ok = TextEdit(dial_fen,2,1,mask,20);
        }
    } while (ok && FIC::dir_open(dirName,mask,&val)<_OK);
    delwin(dial_fen);
    touchwin(mainWin);
    wrefresh(mainWin);
    
    if (ok && val) {
        WINDOW *dir_fen=WindowCreate(gui.layout.dir);
        werase(dir_fen);
        scrollok(dir_fen, TRUE);
        while (val--) {
            FIC::dir_next(dirName);
            scroll(dir_fen);
            mvwaddstr(dir_fen,gui.layout.dir.nl-2,1,dirName);
            if ((tmp = wgetch(dir_fen))!=ERR) {
                gui.Mess("Listing hold... press any key or CTRL-A to stop");
                GetKey(dir_fen);
                if (tmp==1)
                    goto dir_kc;
                gui.ClrMess();
            }
        };
        FIC::dir_close();
        gui.Mess("Press any key to continue...");
        tmp = GetKey(dir_fen);
    dir_kc:
        gui.ClrMess();
        delwin(dir_fen);
        touchwin(mainWin);
        wrefresh(mainWin);
    }
}

// \todo search does not work at block boundaries
static void Search(Gui &gui, FileView &fv)
{
    WINDOW *mainWin(gui.mainWin);
    
    char searchString[80];
    WINDOW *dial_fen=WindowCreate(gui.layout.dial);
    mvwaddstr(dial_fen,1,1,"Hexa string:");
    searchString[0] = 0;
    bool ok = TextEdit(dial_fen,2,1,searchString,40);
    delwin(dial_fen);
    touchwin(mainWin);
    wrefresh(mainWin);
    
    unsigned char bytes[40];
    for (int i=0; i<strlen(searchString); i+=2)
    {
        if (!isxdigit(searchString[i]) || !isxdigit(searchString[i+1])) return;
        bytes[i/2] = ToHex(searchString[i])*16 + ToHex(searchString[i+1]);
    }
    
    if (ok && strlen(searchString) && !(strlen(searchString)&1)) {
        unsigned char tBuff[0x200];
        int lus,i,j,tAddr=fv.addr,found=0;
        do {
            fv.Read(tAddr&0xFFFFFe00,(char *)tBuff,512,&lus);
            for (i=0; !found && i<lus; i++) {
                found=1;
                for (j=0; found&&j<strlen(searchString)/2; j++) {
                    if (tBuff[i+j] != bytes[j])
                        found=0;
                }
            }
            if (found) i--;
            else tAddr += 512;
        } while (!found && lus);
        if (found) fv.addr = (tAddr&0xFFFFFe00)+i;
    }
}

static void Help(Gui &gui)
{
    WINDOW *mainWin(gui.mainWin);
    
    WINDOW *help_fen=WindowCreate(gui.layout.help);
    for (int tmp=0; tmp<gui.layout.help.nl-2; tmp++) {
        mvwaddstr(help_fen,tmp+1,1,help[tmp]);
    }
    gui.Mess("Pressez une touche");
    GetKey(help_fen);
    gui.ClrMess();
    delwin(help_fen);
    touchwin(mainWin);
    wrefresh(mainWin);
    
}

static void Goto(Gui &gui, FileView &fv)
{
    WINDOW *mainWin(gui.mainWin);
    
    char input[80];
    bool ok;
    int val;
    
    gui.Mess("Goto (A)ddress (S)ector (B)eginning (E)nd?");
    int key = WaitForKey(mainWin, "\001AaSsBbEe");
    gui.ClrMess();
    switch(key) {
        case 1:
        case 2:
            gui.Mess("Address :");
            do {
                input[0] = 0;
                ok = TextEdit(mainWin,gui.layout.disp.nl-2,16,input,8);
            } while (ok && !sscanf(input," %x",&val));
            if (ok) {
                if (Refresh(gui, fv, val) < _OK)
                    break;
                fv.addr = val;
            }
            break;
        case 3:
        case 4:
            gui.Mess("Sector  :");
            do {
                input[0] = 0;
                ok = TextEdit(mainWin,gui.layout.disp.nl-2,16,input,6);
            } while (ok && !sscanf(input," %d",&val));
            if (ok) {
                val *= 512;
                if (Refresh(gui, fv, val) < _OK)
                    break;
                fv.addr = val;
            }
            break;
        case 5:
        case 6:
            fv.addr = 0;
            break;
        case 7:
        case 8:
            fv.addr = fv.Size()-1;
            break;
        default:
            break;
    }
    gui.ClrMess();
}

static string DirName(const string &path)
{
    string::const_reverse_iterator p = find(path.rbegin(), path.rend(), '/');
    if (p == path.rend())
        return ".";
    return string(path.begin(), p.base()-1);
}

static string FileName(const string &path)
{
    string::const_reverse_iterator p = find(path.rbegin(), path.rend(), '/');
    if (p == path.rend())
        return path;
    return string(p.base(), path.end());
}

/*
 Editeur de fichier
 */
void theAppl(WINDOW *mainwin, int argc, char *argv[])
{
    FileView fv;
    
    Gui gui(mainwin);
    
    if (argc>1) {
        SwitchToFile(argv[1], fv);
        mvwaddstr(gui.mainWin,1,6,"                                                 ");
        mvwaddstr(gui.mainWin,1,6,fv.FileName());
    }
    else
        OtherFile(gui, fv);
    
    int key=0;
    while (key != 17) { /*CTRL-Q*/
        Refresh(gui, fv, fv.addr);
        key = GetKey(gui.mainWin);
        gui.ClrMess();
        
        // Key aliasing
        int remap[]={KEY_UP,21,KEY_DOWN,4,KEY_LEFT,2,KEY_RIGHT,6,KEY_NPAGE,14,KEY_C3,14,KEY_PPAGE,16,KEY_A3,16,KEY_C1,5,263,8};
        int nbRemap=sizeof(remap)/sizeof(remap[0]);
        for (int i=0; i<nbRemap; i+=2)
            if (key==remap[i])
                key=remap[i+1];
        
        switch(key) {
            case 1 :/*CTRL-A -> Abort*/
                gui.Mess("Annulation des modifications faites sur le fichier ?");
                if (WaitForKey(gui.mainWin,"oOyYnN") < 4) {
                    fv.Close();
                    fs::path tempFile = fs::temp_directory_path() / "patch.tmp";
                    fs::path filePath = fv.Path();
                    fs::remove(filePath);
                    fs::copy(tempFile, filePath);
                    fv.Open(filePath);
                    fv.Dirty();
                    gui.Mess("Ancien etat du fichier restaure                            ");
                }
                else
                    gui.ClrMess();
                break;
            case 2 :/*CTRL-B -> Backward*/
                fv.addr -= 1;
                break;
            case 3 :/*CTRL-C -> User abort*/
                break;
            case 4 :/*CTRL-D -> Down*/
                fv.addr += 16;
                break;
            case 5 :/*CTRL-E -> End of File*/
                fv.addr = fv.Size()-1;
                break;
            case 6 :/*CTRL-F -> Forward*/
                fv.addr += 1;
                break;
            case 7 :/*CTRL-G -> Goto*/
                Goto(gui, fv);
                break;
            case 8 :/*CTRL-H -> Help*/
                Help(gui);
                break;
            case 9 :/*CTRL-I*/
                break;
            case 10:/*CTRL-J -> Jump to mark*/
                if (fv.mark > 0)
                    fv.addr = fv.mark;
                break;
            case 11:/*CTRL-K*/
                break;
            case 12:/*CTRL-L -> List files*/
                ListFiles(gui);
                break;
            case 13:/*CTRL-M -> Mark*/
                fv.mark = fv.addr;
                gui.Mess("Mark set");
                break;
            case 14:/*CTRL-N -> Next page*/
                fv.addr += 256;
                break;
            case 15:/*CTRL-O -> Other file*/
                OtherFile(gui, fv);
                break;
            case 16:/*CTRL-P -> Prev page*/
                fv.addr -= 256;
                break;
            case 17:/*CTRL-Q -> Quit*/
                break;
            case 18:/*CTRL-R -> Re-read sector*/
                fv.Dirty();
                gui.Mess("Sector re-read");
                break;
            case 19:/*CTRL-S*/
                Search(gui, fv);
                break;
            case 20:/*CTRL-T -> Toggle mode*/
                fv.ToggleAscii();
                break;
            case 21:/*CTRL-U -> Up*/
                fv.addr -= 16;
                break;
            case 22:/*CTRL-V*/
                break;
            case 23:/*CTRL-W -> Write sector*/
                fv.Write(512 * (fv.addr/512), fv.data, 512);
                gui.Mess("Sector written");
                break;
            case 24:/*CTRL-X*/
                break;
            case 25:/*CTRL-Y*/
                break;
            case 26:/*CTRL-Z*/
                break;
            default:
                if (key>=32 && key<=126)
                    Edit(gui,fv,fv.addr,key);
                break;
        }
    }
    
    fs::path tempFile = fs::temp_directory_path() / "patch.tmp";
    if (fs::exists(tempFile))
        fs:remove(tempFile);
}




/*
 Edition d'un champ de texte.
 PARAMETRES D'ENTREE :
 -window	: identificateur de la fenetre ou se deroule l'edition.
 -row,col: emplacement dans la fenetre.
 -text	: texte a editer.
 -nbchar	: largeur du champ de l'edition
 
 Return: true or false if the user cancelled the input
 */  
bool TextEdit(WINDOW *window, int row, int col, char *text, int nbchar)
{
    int	key,i;
    
    int index=strlen(text);
    for (i=0; i<nbchar && text[i]; i++);
    for (; i<nbchar; i++)
        text[i]=' ';
    text[i]=0;
    do {
        if (mvwaddstr(window,row,col,text) < _OK)
            FAIL("curses");
        if (index>=nbchar)
            index = nbchar-1;
        if (wmove(window,row,col+index) < _OK)
            FAIL("curses");
        wrefresh(window);
        key = GetKey(window);
        
        switch(key) {
            case 1:
                return false;
            case KEY_RIGHT:
                if (index<nbchar-1)
                    index++;
                break;
            case KEY_LEFT:
                if (index>0)
                    index--;
                break;
            case KEY_BACKSPACE:
            case KEY_DC:
            case 0x7F: /*DEL*/
                if (index>0)
                    index--;
                for (i=index; i<nbchar; i++)
                    text[i]=' ';
                break;
            default:
                if (key<128 && key>32) {
                    text[index]=key;
                    index++;
                }
                break;
        }
    } while (key != 13);
    for (i=0; text[i] && text[i]!=' '; i++);
    text[i]=0;
    return true;
}


int WaitForKey(WINDOW *window, const char *set)
{
    int	index;
    int len = strlen(set);
    do {
        int key;
        key = GetKey(window);
        for (index=0; index<len && set[index]!=key; index++);
    } while (index == len);
    
    return index;
}

WINDOW *WindowCreate(const WIN_DEF &def)
{
    WINDOW *win = newwin(def.nl, def.nc, def.l, def.c);
    nodelay(win, TRUE);
    keypad(win, TRUE);
    scrollok(win, FALSE);
    box(win,0,0);
    return win;
}

static int ToHex(int c)
{
    if (c>='0' && c<='9') return c-'0';
    if (c>='a' && c<='f') return c-'a'+10;
    if (c>='A' && c<='F') return c-'A'+10;
    FAIL("Non hex char");
}

static int HexDigit(int val)
{
    return (val > 9)? ('A'+val-10): ('0'+val);
}

static void HexPrint(char *add, int val)
{
    *(add) = HexDigit(((val)>>4)&0x0F);
    *((add)+1) = HexDigit((val)&0x0F);
}

/*
 Modification de la valeur d'un octet
 */
static void Edit(Gui &gui, FileView &fv, int &addr, int key)
{
    WINDOW *window(gui.mainWin);
    
    char val[3];
    int row,col,tmp;
    
    if (!fv.Ascii())
    {
        if (!isxdigit(key)) return;
        row = (addr & 0xFF) /16 + 4;
        col = (addr & 0x0F) * 3 + HEX_COL;
        val[0] = key;
        val[1] = 0;
        mvwaddstr(window,row,col,val);
        wmove(window,row,col+1);
        tmp = key;
        do {
            key = GetKey(window);
        } while(!isxdigit(key));
        val[0] = key;
        mvwaddstr(window,row,col+1,val);
        key = ToHex(key) + (ToHex(tmp) << 4);
        fv.data[addr & 0x1FF] = key;
        Refresh(gui, fv, addr);
    }
    fv.data[addr & 0x1FF] = key;
    Refresh(gui, fv, addr);
    
    addr++;
    Refresh(gui, fv, addr);
}


/*
 Affiche un demi-secteur
 */
static int Refresh(Gui &gui, FileView &fv, int &addr)
{
    WINDOW *window(gui.mainWin);
    
    // previous state
    static int old_sect, old_page, old_line;
    
    int i,j, curs_l,curs_c,curs_c2, base, sect,page,line;
    char txt[81], val[10], crnt, key;
    
    if (fv.IsDirtyAndReset()) {
        old_sect = -1;
    }
    
    if (addr < 0)
        addr = 0;
    
    sect = addr >> 9;
    
    // new sector
    if (sect != old_sect) {
        int lus;
        fv.Read(sect*512,fv.data,512,&lus);
        if (!lus) {
            if (sect == 0)
                goto error0;
            addr = addr & 0xFFFFFE00;
            addr--;
            sect = addr >> 9;
            fv.Read(sect*512,fv.data,512,&lus);
        }
        while (lus < 512)
            fv.data[lus++] = 0;
    }
    
    page = (addr & 0x100)? 1: 0;
    line = (addr & 0xF0) >> 4;
    
    if (page != old_page || sect != old_sect) {
        old_page = page;
        old_sect = sect;
        old_line = 0;
    }
    
    sprintf(txt,"Sect:%6d  Page:%1d  Addr:%08X", sect, page, addr);
    mvwaddstr(window,2,1,txt);
    
    curs_l = (addr & 0xFF) / 16 + 4;
    curs_c = (addr & 0x0F) * 3 + HEX_COL;
    curs_c2 = (addr & 0x0F) + ASC_COL;
        
    if ((key=wgetch(window))!=ERR) {
        ungetch(key);
        goto broken;
    }
    
    if (line < old_line) {
        crnt = fv.data[addr & 0x1FF];
        HexPrint(val,crnt);
        val[2] = 0;
        mvwaddstr(window,curs_l,curs_c,val);
        if (crnt<32 || crnt>126)
            crnt = '.';
        sprintf(val,"%c",crnt);
        mvwaddstr(window,curs_l,curs_c2,val);
    }
    
    
    if (old_line < 16) {
        char *buff = fv.data+(page << 8);
        
        int nCols = getmaxx(window)-getbegx(window)+1;
        for (i=0; i<nCols; i++)
            txt[i]=' ';
        txt[i]=0;
        for (; old_line<16; old_line++) {
            if ((key=wgetch(window))!=ERR) {
                ungetch(key);
                goto broken;
            }
            base = sect * 512 + page * 256 + old_line * 16;
            sprintf(txt,"%08X-> ",base);
            txt[11] = ' ';
            for (j=0; j<16; j++) {
                crnt = buff[16*old_line + j];
                HexPrint(&txt[HEX_COL-1+j*3],crnt);
                if (crnt < 32 || crnt>126)
                    txt[ASC_COL-1+j] = '.';
                else
                    txt[ASC_COL-1+j] = crnt;
            }
            if (mvwaddstr(window,4+old_line,1,txt) < _OK)
                goto error2;
        }
    }
    
broken:
    if (fv.Ascii()) {
        swap(curs_c, curs_c2);
    }
    if (wmove(window,curs_l,curs_c) < _OK)
        goto error2;
    wrefresh(window);
    
    /* Sortie sans erreur                                                   */
    /* ------------------                                                   */
    ERR_OK();
    
    /* Traitement des erreurs                                               */
    /* ----------------------                                               */
error0:
    ERROR(_FAIL,"null file");
error1:
    ERROR(_FAIL,"file read");
error2:
    ERROR(_FAIL,"scr write");
    
    ERR_EXIT("ref");
}
