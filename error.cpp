//
//  error.cpp
//  filePatch
//
//  Created by Brice Rivé on 07/10/12.
//  Copyright (c) 2012 Brice Rivé. All rights reserved.
//
#include "error.h"
#include <iostream>
using namespace std;

Err::Err(const char *file, const char *func, int line, const char *msg)
{
    mText = string(file)+"/"+string(func)+ " error: "+msg;
}
