//
//  error.h
//  filePatch
//
//  Created by Brice Rivé on 07/10/12.
//  Copyright (c) 2012 Brice Rivé. All rights reserved.
//
#include <string>
#define _OK 0
#define _ERROR -1
#define ERR_OK() return _OK
#define ERROR(type,msg) printf("%s Error: %s\n",__FILE__,msg)
#define ERR_EXIT(msg) printf("%s %s\n",__FILE__,msg); return _ERROR;

#define _WHERE __FILE__, __FUNCTION__, __LINE__
#define FAIL(msg) throw Err(_WHERE, msg);

class Err
{
public:
    Err(const char *file, const char *func, int line, const char *msg);
    std::string mText;
};