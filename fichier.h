//
//  fichier.h
//  filePatch
//
//  Created by Brice Rivé on 07/10/12.
//  Copyright (c) 2012 Brice Rivé. All rights reserved.
//
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdlib.h> /* free() */
#include <string.h> // strcpy

class FIC
{
public:
    static int dir_open(const char *d, const char *m, int *n);
    static int dir_next(char *d);
    static void dir_close();
};
