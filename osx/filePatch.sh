#!/bin/sh
# filePatch starter script
# Using Platypus to generate a drag'n drop app that starts filePatch in a new iTerm Window


function iterm () {
    osascript &>/dev/null <<EOF
        tell application "iTerm2"
            set newWindow to (create window with default profile)
            tell current session of newWindow
                write text "filePatch \"$@\""
            end tell
        end tell
EOF
}
echo $@
iterm $@